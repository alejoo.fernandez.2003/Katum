# Contributing

Any contribution is appreciated.
Let's make Katun the best educational music rhythm software!
To contribute, you can do so from GitHub, or GitLab.
## Contributor License Agreement

By making a change to this repository, you grant the creator a non-exclusive, sublicensable, and irrevocable license to store data in the repository.
The license acknowledges that you are the author of the software.
